﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Зав1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        TextBox textBox;
        Button button;
        PictureBox picture;
        List<string> question;
        List<string> answer;
        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            question = new List<string>();
            answer = new List<string>();

            QuestionAnswer.FileReadToQuestion(question);
            QuestionAnswer.FileReadToAnswer(answer);

            label2.Text = i.ToString();
            label1.Text = question[0];

            textBox = new TextBox();
            textBox.Width = 200;
            textBox.Height = 26;
            textBox.Top = 180;
            textBox.Left = 180;
            textBox.Font = new Font(textBox.Font.Name, 10F);
            this.Controls.Add(textBox);

            button = new Button();
            button.Text = "Відповісти";
            button.Width = 150;
            button.Height = 50;
            button.Top = 240;
            button.Left = 260;
            button.Click += button1_Click;
            this.Controls.Add(button);

            picture = new PictureBox();
            picture.Height = 170;
            picture.Width = 150;
            picture.Left = 5;
            picture.Top = 70;
            picture.SizeMode = PictureBoxSizeMode.Zoom;
            picture.ImageLocation = "https://www.jnsm.com.ua/ures/img/hphoto/Einstein_Toung.jpg";

            timer1.Enabled = true;
        }

        int i = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            i++;
            label2.Text = i.ToString();
            if (i == 15)
            {
                timer1.Stop();
                label2.ForeColor = Color.Red;
                label1.Text = "Для початку натисніть \"Restart\"";
                this.Controls.Remove(picture);
                textBox.Clear();
                MessageBox.Show("Ви програли \nДля початку натисніть \"Restart\"");
            }
        }

        int n = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if (label1.Text == question[n] && textBox.Text == answer[n])
            {
                i = 0;
                label2.Text = i.ToString();
                textBox.Clear();
                n++;
                if(n < question.Count)
                {
                    label1.Text = question[n];
                }
                else
                {
                    timer1.Enabled = false;
                    label1.Text = "Ви виграли! \nДля початку натисніть \"Restart\"";
                    this.Controls.Remove(picture);
                    MessageBox.Show("Ви виграли! \nДля початку натисніть \"Restart\"");
                }
            }

            else
            {
                MessageBox.Show("Невірно");
                textBox.Clear();
            }

            if (label1.Text == "Хто зображений на картинці?")
            {
                this.Controls.Add(picture);
            }
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            label2.ForeColor = Color.Red;
            label1.Text = "Для початку натисніть \"Restart\"";
            this.Controls.Remove(picture);
            textBox.Clear();
            MessageBox.Show("Гра зупинена \nДля початку натисніть \"Restart\"");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            n = 0;
            i = 0;
            label2.Text = i.ToString();
            label2.ForeColor = Color.LimeGreen;
            label1.Text = question[0];
            this.Controls.Remove(picture);
            textBox.Clear();
            timer1.Enabled = true;
        }
    }
}
